package password;

/**
 * 
 * @author Mert Havza 991538291
 *
 */

public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	public static boolean hasValidCaseChars(String password) {
		return password != null && password.matches(".*[A-Z]+.*") && password.matches(".*[a-z]+.*"); 
	}
	
	public static boolean isValidType(String password) {
		
		int digitCounter = 0;
		
		char[] chars = password.toCharArray();
		
		for(char c : chars) {
			if(Character.isDigit(c)) {
				digitCounter++;
			}
		}
		
		if(digitCounter >= 2) {
			return true;
		}
		
		else {
			return false;
		}
		
		
	}

}
