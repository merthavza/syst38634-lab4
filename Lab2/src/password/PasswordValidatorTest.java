package password;

import static org.junit.Assert.*;

/**
 * 
 * @author Mert Havza 991538291
 *
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("dfugdwifGGiui"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue("Invalid case chars", PasswordValidator.hasValidCaseChars("Aa"));
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("345678"));
	}
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(""));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutLower() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("a"));
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryOutNull() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars(null));
	}
	
	////////////////////////////////////////////////////////////////////////////

	@Test
	public void testIsValidLengthRegular() {
		
		boolean passLength = PasswordValidator.isValidType("HelloWorld");
		
		assertTrue("Invalid Password Length", passLength);
	}
	
	@Test
	public void testIsValidLengthException() {
		
		boolean passLength = PasswordValidator.isValidType("Hello");
		
		assertFalse("Invalid Password Lenght", passLength);
	}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
		
		boolean passLength = PasswordValidator.isValidType("HelloMert");
		
		assertTrue("Invalid Password Lenght", passLength);
	}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
		
		boolean passLength = PasswordValidator.isValidType("Hello12");
		
		assertFalse("Invalid Password Lenght", passLength);
	}
	
	@Test
	public void testIsValidTypeRegular() {
		
		boolean passContainsDigit = PasswordValidator.isValidType("Hello45Mert");
		
		assertTrue("Password must contain at least 2 digits", passContainsDigit);
	}
	
	@Test
	public void testIsValidTypeException() {
		
		boolean passContainsDigit = PasswordValidator.isValidType("NoDigitPass");
		
		assertFalse("Password must contain at least 2 digits", passContainsDigit);
	}
	
	@Test
	public void testIsValidTypeBoundaryIn() {
		
		boolean passContainsDigit = PasswordValidator.isValidType("20PassWithDigit");
		
		assertTrue("Password must contain at least 2 digits", passContainsDigit);
	}
	
	@Test
	public void testIsValidTypeBoundaryOut() {
		
		boolean passContainsDigit = PasswordValidator.isValidType("1DigitPass");
		
		assertFalse("Password must contain at least 2 digits", passContainsDigit);
	}

}
